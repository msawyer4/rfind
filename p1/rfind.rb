#!/usr/bin/env ruby

# Set regex as command line argument
regex = ARGV[0]

# Print out all files that match regex (only ruby, web and text files)
puts "Files with names that matches <#{regex}>"
IO.popen("find . -type f -print | egrep '#{regex}\.(rb|erb|js|css|html|\
    yml|txt)'") do |results|
    results.each do |filename|
        puts '  ' + filename
    end
end

puts "**************************************************"
puts "Files with content that matches <#{regex}>"

ioArray = IO.popen("rgrep -i -l #{regex} * | egrep '.*\.(rb|erb|js|css|html|yml|txt)' | sort").readlines
ioArray[0...-1].each do |file|
    puts "./#{file}"
    IO.popen("grep -i -n #{regex} #{file}") do |resultlist|
        resultlist.each do |result|
            puts '  ' + result
        end
    end
    puts "--------------------------------------------------"
end
puts "./#{ioArray[ioArray.size-1]}"
IO.popen("grep -n #{regex} #{ioArray[ioArray.size-1]}") do |resultlist|
    resultlist.each do |result|
        puts '  ' + result
    end
end
